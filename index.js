const fastify = require('fastify')({logger: true});

fastify.get('/', async (request, reply) => {
  reply.send(`<h1>Hello Fastify!</h1>`)
})

const start = async () => {
  try {
    await fastify.listen(3000);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
}

start()